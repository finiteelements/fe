% Material and section data
% -------------------------
%
% From "Structural vibration of flexural beams with thick unconstrained
% layer damping"

b = 0.01;         % width of section
h = 0.004;        % heigh of section
Lt = 0.12;        % length of beam
rho = 7782;       % density 
A = b*h;          % area of section
E = 176.2E9;      % Young's modulus
I = 1/12*b*h^3;   % inertia