% Time response of beam 
% ----------------------

% Time response of a beam clamped or simply supported in and end and with
% damper/spring/damper + spring in its free end

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 200;    % number of nodes
dof = 2;   % number of dof/node

% Simulation time

T = 1; % simulation time
samples = 1000;
Fs = samples/T;

% Material and section data

r = 0.01;         % radius of section
Lt = 1;           % length of beam
rho = 7900;       % density 
A = pi*r^2;       % area of section
E = 210E9;        % Young's modulus
I = pi/2*r^4;     % inertia

L = Lt/(N-1); % element length

% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);


% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 4);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N+1);
M = assemble(me, connect, dof*N+1);

% Support
% -------

k = 1e6;

% Position
i = [length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [length(K)-2 length(K) length(K)-2 length(K)];
v = [k -k -k k];

Ks = sparse(i,j,v,size(K,1),size(K,1));

K = K + Ks;

Kuu = K(1:end-1,1:end-1);
Kub = K(1:end-1,end);

Muu = M(1:end-1,1:end-1);
Mub = M(1:end-1,end);

% Damping matrix
% --------------

c = 1e3;
% c = 0; % TEST: no damper

% Position
i = [length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [length(K)-2 length(K) length(K)-2 length(K)];
v = [c -c -c c];

C = sparse(i,j,v,size(K,1),size(K,1));

Cuu = C(1:end-1,1:end-1);
Cub = C(1:end-1,end);

% Boundary conditions
% --------------------

% Clamped in first node
BC = [1 1 0; 1 2 0]; % [node dof value]

% Simply supported in first node
% BC = [1 1 0]; % [node dof value]


M_BC = applyBC(Muu, BC);
K_BC = applyBC(Kuu, BC);
C_BC = applyBC(Cuu,BC);

% Response
% -------- 

% Initial deformation
ub = 3e-3;
Kub = applyBC(Kub,BC);
X0 = -K_BC\(Kub*ub);

% Initial velocity
dotX0 = zeros(size(X0));

% Initial acceleration
ddotX0 = -M_BC\(C_BC*dotX0 + K_BC*X0);

% Newmark coefficients
delta = 0.5;
alpha = 0.25*(0.5+delta)^2;
[X, dotX, ddotX] = newmarkIC({M_BC, C_BC, K_BC}, {X0, dotX0, ddotX0}, T, samples, delta, alpha);

X = assembleBC(X, BC);
dotX = assembleBC(dotX, BC);
ddotX = assembleBC(ddotX, BC);

% Response in a single node and DOF
% Only linear movement
node = N;
map = (node-1)*2 + 1;

Xn = X(map,:);
dotXn = dotX(map,:);
ddotXn = ddotX(map,:);

% Displacement
fig = plot(linspace(0,T,samples),Xn);
xlabel('t'), ylabel('x(t)')
[FFT_Data, f_FFT] = espectro(full(Xn), Fs);
figure
fig = plot(f_FFT,abs(FFT_Data));
xlabel('f'), ylabel('X(f)')

% Speed
figure
fig = plot(linspace(0,T,samples),dotXn);
xlabel('t'), ylabel('v(t)')

% Acc
figure
fig = plot(linspace(0,T,samples),ddotXn);
xlabel('t'), ylabel('a(t)')

% Natural frequencies
% -------------------

% Compare peaks to natural frequencies
[phi, lambda] = eigs(K_BC,M_BC,30,'smallestabs');
lambda = sqrt(diag(lambda));
lambda = lambda/(2*pi); % Hz
    
% Animation
% ---------

% Maximum displacement 
maxX = max(max(abs(X(1:2:end,:))));

figure(5)

file = 'modes/clampedTimeSeismic.gif';
for i = 1:samples 
    figure(5) % to avoid plotting in the current figure
    fig = plot(X(1:2:end,i));
    xlabel('L'), ylabel('x(t)')
    axis([1 N -2*maxX 2*maxX]);
    drawnow
    pause(0.1)
    
    frame = getframe(gcf);
    img =  frame2im(frame);
    [img,cmap] = rgb2ind(img,256);
    if i == 1
        imwrite(img,cmap,file,'gif','LoopCount',Inf,'DelayTime',0.1);
    else
        imwrite(img,cmap,file,'gif','WriteMode','append','DelayTime',0.1);
    end
end