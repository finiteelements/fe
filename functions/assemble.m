function A = assemble(elementary, connectivity, N)
% Assembles elementary matrices to form system matrix.
%
% A = ASSEMBLE(elementary, connectivity, N)
% 
% where:
% - elementary: elementary matrix
% - connectivity: connectivity of the model
% - N: total size of the model

A = sparse(N, N);

% Dof mapping
% As there are two dofs in each node

% map = [a b], the two nodes that create the element
% NOTE: To extend to any dof/node instead of 2 
% - number of dof in each node, input to the function
% map = [a b c ...] where size(map,2) = nodes in each element

map = (connectivity-1)*2 + 1;

% For each element
for t=1:size(map,1)
   
    % Positions 
    % NOTE: to extend to any dof/node 
    % pos = [a a+1 ... a+(dof/node-1) b ...]
    
    % pos = [a a+1 b b+1];
    pos = [map(t,1) map(t,1)+1 map(t,2) map(t,2)+1]; 
    
    % Positions of elements of elementary matrix in complete matrix
    
    %[(a,a)    (a, a+1)   | (a,b)    (a, b+1);
    % (a+1, a) (a+1, a+1) | (a+1, b) (a+1, b+1);
    % ------------------------------------------
    % (b,a)    (b, a+1)   | (b,b)    (b, b+1);
    % (b+1, a) (b+1, a+1) | (b+1,b)  (b+1, b+1)]
    
    % row of elements: i = a a a a a+1 a+1 a+1 a+1 b b b b b+1 b+1 b+1 b+1
    % column: j = a a+1 b b+1 a a+1 b b+1 a a+1 b b+1 a a+1 b b+1
    
    % If a row of pos is taken and repeated lenght(elementary) times:
    % a a+1 b b+1
    % a a+1 b b+1
    % a a+1 b b+1
    % a a+1 b b+1
    
    % making a vector with the columns gives i
    % making a vector with the rows gives j
    
    pos1 = reshape(repmat(pos,length(elementary),1), [1 length(elementary)^2]);
    pos2 = reshape(repmat(pos,1,length(elementary)), [1 length(elementary)^2]);
    
    A = A + sparse(pos1, pos2, elementary(:), N, N);
end

end