function A = symmetrise(B)
% Creates symmetric matrix from the elements in the upper triangle
%
% A = SYMMETRISE(B)
%
% See also TRIU, TRIL

% Lower triangular part equal to upper
% Don't take into account diagonal again
A = B + tril(B',-1);
end