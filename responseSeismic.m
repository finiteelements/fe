% Computation of response
% ------------------------
%
% Computes response of beam supported in two bearings following fractional
% models

clear
close all
clc

% Add current folders and subfolders to path
addpath(genpath(pwd))

% Data
% ----

% Data for FE

N = 20;    % number of nodes
dof = 2;   % number of dof/node

% Material and section data

r = 0.005;        % radius of section
Lt = 1;           % length of beam
rho = 7900;       % density 
A = pi*r^2;       % area of section
E = 210E9;        % Young's modulus
I = pi/2*r^4;     % inertia

L = Lt/(N-1);     % element length


% Interpolation functions
% -----------------------

[M_N, K_N] = interpFunc(L);

% Gaussian quadrature
% -------------------

% L/2 to adjust interval from [-1 1] to [0 L] 

ke = L/2*gaussIntegrate(K_N, 2);
ke = E*I*ke;

me = L/2*gaussIntegrate(M_N, 2);
me = rho*A*me;

% Connectivity
% -------------
% The rows represent the nodes the element connect
%
% For the beam: elements are connected to the next one 
connect = [(1:(N-1))' (2:N)'];

K = assemble(ke, connect, dof*N+1);
M = assemble(me, connect, dof*N+1);

% Support
% -------

k1 = 1e6;
k2 = 1e6;

% Position
i = [1 length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [1 length(K)-2 length(K) length(K)-2 length(K)];
v = [k1 k2 -k2 -k2 k2];

Ks = sparse(i,j,v,size(K,1),size(K,1));

K = K + Ks;

Kuu = K(1:end-1,1:end-1);
Kub = K(1:end-1,end);

Muu = M(1:end-1,1:end-1);
Mub = M(1:end-1,end);

% Damping matrix
% --------------

c1 = 1e6;
c2 = 1e6;

% Position
i = [1 length(K)-2  length(K)-2 length(K) length(K)]; % K has N+1 dofs
j = [1 length(K)-2 length(K) length(K)-2 length(K)];
v = [c1 c2 -c2 -c2 c2];

C = sparse(i,j,v,size(K,1),size(K,1));

Cuu = C(1:end-1,1:end-1);
Cub = C(1:end-1,end);

% Seismic response
ub = 1e-3;
dotub = 0;
ddotub = 0;

F = -Kub*ub -Cub*dotub -Mub*ddotub;

% Boundary conditions
% --------------------

% Free
BC = [];

M_BC = applyBC(Muu, BC);
C_BC = applyBC(Cuu, BC);
K_BC = applyBC(Kuu, BC);
F_BC = applyBC(F, BC);


% Response
% --------

% Frequecy vector
steps = 1000;
fMin  = 0;
fMax  = 10000*2*pi;

f = linspace(fMin, fMax, steps); % rad/s

% Impulse load
P = ones(steps,1);

% Precondition with unit matrix
I = eye(size(M_BC),'like', M_BC);

% Initialisation
x = zeros(length(I),steps);

x0 = x(:,1); 

for n = 0:length(f)-1 
    
    s = 1i*f(n+1);
    Z = M_BC*s^2 + C_BC*s + K_BC;
    % Z = M_BC*s^2 + K_BC; TEST: no damping

    F = F_BC*P(n+1);

    [xk,~] = bicg(Z,F,1e-9,1e6,I,I,x0); % biconjugate gradients method 
    x(:,n+1) = xk;
    x0 = xk; % use x0 = x_k-1
    
end

% Assemble x
x = assembleBC(x, BC);

% RMS response of all points (RMS of modulus)
x = x(1:2:end,:); % each column is the response at a certain f
rms = mean(abs(x).^2).^0.5; % mean is done in every column

figure
semilogy(f,rms);
title('RMS ')
xlabel('\omega (rad/s)'), ylabel('Amplitude (m)')
xlim([0,max(f)])

% Eigenvalues
modeN = 10;
[phi, lambda] = eigs(K_BC,M_BC,modeN,'smallestabs');

lambda = real(sqrt(diag(lambda)));

% Save data 
% ---------

% TODO

% NOTES
%
% - Peaks agree with natural frequencies